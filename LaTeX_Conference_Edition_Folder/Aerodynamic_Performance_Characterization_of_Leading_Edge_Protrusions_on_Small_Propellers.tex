
%Kevin Moore Sci-Tec Extended Abstract for June 1st 2015
%Use AIAA Template
\immediate\write18{makeindex \jobname.nlo -s nomencl.ist -o \jobname.nls}
\documentclass[]{aiaa-tc}% insert '[draft]' option to show overfull boxes

%Include appropriate packages including eps to pdf
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{placeins}
\usepackage[super]{cite}
\usepackage{subfigure}
\usepackage[english]{babel}
\usepackage{indentfirst}
\usepackage{varioref}% smart page, figure, table, and equation referencing
\usepackage{wrapfig}% wrap figures/tables in text (i.e., Di Vinci style)
\usepackage{threeparttable}% tables with footnotes
\usepackage{dcolumn}% decimal-aligned tabular math columns
\newcolumntype{d}{D{.}{.}{-1}}
\usepackage{nomencl}% nomenclature generation via makeindex
\makenomenclature
\usepackage{subfigure}% subcaptions for subfigures
\usepackage{subfigmat}% matrices of similar subfigures, aka small mulitples
\usepackage{fancyvrb}% extended verbatim environments
\fvset{fontsize=\footnotesize,xleftmargin=2em}
\usepackage{lettrine}% dropped capital letter at beginning of paragraph
\usepackage[colorlinks]{hyperref}% hyperlinks [must be loaded after dropping]
\hypersetup{colorlinks,breaklinks,
citecolor = red,
urlcolor=red,
linkcolor=red}


%Format section and subsections to be 1.1.1 etc
\renewcommand{\thesection}
{\arabic{section}}

\renewcommand{\thesubsection}
{\thesection.\arabic{subsection}}



\title{Aerodynamic Performance Characterization of Leading Edge Protrusions on Small Propellers}

\author{\
Kevin R. Moore\thanks{Undergraduate Researcher, Department of Mechanical Engineering, AIAA Student Member}
\ and Andrew Ning\thanks{Assistant Professor, Department of Mechanical Engineering, AIAA Senior Member}\\
{\normalsize\itshape
U of U Young University, Provo, UT, 84602, USA}\
\\
\
{
}
}

% Data used by 'handcarry' option
\AIAApapernumber{YEAR-NUMBER}
\AIAAconference{AIAA SciTech, Jan 4-8 2016, San Diego CA}
\AIAAcopyright{\AIAAcopyrightD{2016}}

% Define commands to assure consistent treatment throughout document
\newcommand{\eqnref}[1]{(\ref{#1})}
\newcommand{\class}[1]{\texttt{#1}}
\newcommand{\package}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{#1}}
\newcommand{\BibTeX}{\textsc{Bib}\TeX}

\begin{document}

\maketitle

\begin{abstract}
The relatively large tubercles or protruding bumps on the leading edge of the humpback whale fin create counter-rotating chordwise vortices. At high Reynolds numbers, these vortices have been shown to delay stall with minimal effect on drag. The range of Reynolds numbers that these types of vortices form is unknown, and so an investigation of the potential for an increased envelope of operation for small aircraft propellers was done. For the cases tested in this report no benefits were observed. The addition of leading edge protrusions decreased propeller performance in both efficiency and thrust. Because of the inherent restrictions on propeller chord length and wind speeds due to rotational speed, which in turn limits the Reynolds number, it is unlikely that the type of modification tested in this report will be beneficial for small aircraft propellers typical of unmanned aerial vehicles. \\\\\

\end{abstract}

\printnomenclature


\newpage
\section{Introduction}
\label{Introduction}
\lettrine[nindent=4pt]{L}{eading} edge protrusions, or tubercles, have been shown to delay stall and increase efficiency of winged geometries\cite{fish:11icb}. To the author's best knowledge, the effect of leading edge protrusions on aircraft propellers has not been formally investigated to date. As the interface between rotational power and forward thrust, aircraft could benefit from propellers that perform at a higher efficiency or at higher relative angles of attack. This is particularly applicable for small unmanned vehicles where small mass-produced propellers are in heavy use and such a simple change could result in significant benefits.  

Data has been collected for small wind turbines and has shown the use of leading edge protrusions to produce positive results. With higher angles of attack before stall and increased lift coefficients, wind turbines can operate more effectively at lower wind speeds. In a study done on Wenvor wind turbine blades using the method, the annual energy production was increased by 20.6\%.\cite{howle:09ww} A similar increase in either efficiency or power for a propeller could have significant effects on performance and or endurance.  The unknown this report tries to address is whether the benefits seen in studies like this wind turbine this will translate to small propellers where the Reynolds number is significantly lower.

Leading edge protrusions have been found to produce chordwise vortices unique to any other modification. Particle image velocemetry has been used to identify counter-rotating chordwise vortices downstream from the leading edge. \cite{stanway:08mit} These vortices were observed to be in the plane parallel to the top surface of the foil and increased in strength as the angle of attack increased from angles in the range of 10$^{\circ}$ to 18$^{\circ}$.

In swept wings, wing fences are used to decrease spanwise flow. Wing fences are physical plates that extend in the direction of airflow along the top of wings and decrease tip stall by decreasing tip loading from spanwise flow. Decreased spanwise flow can increase efficiency as more flow is directed to produce thrust. Besides physical plates, non-physical methods can also achieve similar effects. Vortex generators have been found to create chordwise vortices that also redirect spanwise flow\cite{swatton:11pf}. Somewhat similar to vortex generators, leading edge protrusions have also been believed to reduce spanwise flow.\cite{murray:07joa}

In one hobbyist report, a crude measurement system showed a small increase in static thrust at the expense of efficiency\footnote{hobbyist report found on \href{http://www.flyinggiants.com/forums/showthread.php?s=82d9cb9b31c1e89327cc1a1180991f7f&t=68698}{www.flyinggiants.com}, accessed 11/25/15}.   This gives some hope that the benefits of leading edge protrusions will extend into lower Reynolds numbers. However, the results of this paper show otherwise for the specific cases tested. 



\begin{figure}
\begin{subfigmatrix}{2}% number of columns
\subfigure[Vortex Generators creating chordwise vorticies in the y-z plane. Figure from the dissertation Effect of Leading Edge Tubercles on Airfoil Performance\cite{hansen:12ua}.]{\includegraphics[width=.63\textwidth]{vortex_generators}\label{f:comparisona}}
\subfigure[Leading Edge Protrusions creating chordwise vorticies in the x-z plane. Figure from the Annual Review of Fluid Mechanics journal\cite{fish:06icb}.]{\includegraphics[height=5cm]{chordwise_vorticities}\label{f:comparisonb}}
\end{subfigmatrix}
\caption{Comparison of vortex generators and leading edge protrusions.}
\label{f:comparison}
\end{figure} 

\section{Hypothesized Effects on Propellers}
\label{Hypothesized Full Range of Affects on Propellers}

Two factors that have the potential to affect propeller performance are re-energizing the boundary layer, to delay flow separation, and the wing fence effect caused by chordwise vortices. Vortex generators, small protruding fins on the airfoil surface as seen in Fig.~\ref{f:comparisona}, are similar to leading edge protrusions in that they have been successfully used to delay flow separation \cite{Lin:91aiaa}. However, vortex generators create chordwise vortices along the x-axis as shown in Fig.~\ref{f:comparisona}. These vortices delay separation by re-energizing the boundary layer as well as acting like wing fences that redirect spanwise flow. Leading edge protrusions create counter-rotating chordwise vortices but along the y-axis as shown in Fig.~\ref{f:comparisonb}. These y-axis vortices also re-energize the boundary layer but have the potential to act as a more effective wing fence. Fluid traveling in the spanwise direction could possibly be caught in one of the y-axis chordwise vortices and more forcefully be redirected in the chordwise direction. The positive effects on sidewash were first suggested in the international patent for tubercle leading edge rotor design \cite{dewar:05pat}. However, data has not been collected to date. 

Significant data has been taken on the lift to drag ratio versus angle of attack for non-rotating wings\cite{custodio:07,akram,feng,sousa}. Although propeller dynamics have notable differences to wing dynamics, some conclusions based on wing performance can be made. Like a conventional wing, a propeller encounters a wide range of angles of attack under normal operation. This is caused by the changing relative angle of attack as the advance speed or propeller rotational speed is changed. This can be quantified by the advance ratio which is defined as the advance speed divided by the propeller tip speed as seen in Eq.~\eqnref{e:advance_ratio}.  The wind tunnel corrected free stream velocity\cite{glauert:1926} due to blockage is defined in Eq.~\eqnref{e:corrected_velocity} and the  Glauert correction variable is defined in Eq.~\eqnref{e:glauert}.

\begin{equation}
\label{e:advance_ratio}
J=\dfrac{V'_{\infty}}{{n}D}
\end{equation} 

\nomenclature{$J$}{advance ratio}%
\nomenclature{$V'_{\infty}$}{corrected free stream velocity, m/s}%
\nomenclature{$D$}{propeller diameter, m}%



\begin{equation}
\label{e:corrected_velocity}
V'_{\infty}=V_{\infty}\Bigg[{1-\dfrac{\tau\bigg(\dfrac{A_{P}}{A_{WT}}\bigg)}{2\sqrt{1+2\tau}}}\Bigg]
\end{equation} 

\nomenclature{$\tau$}{Glauert correction variable}%
\nomenclature{$V_{\infty}$}{measured free stream velocity, m/s}%
\nomenclature{$A_{WT}$}{wind tunnel cross sectional area, m\textsuperscript{2}}%
\nomenclature{$A_{P}$}{propeller disk area, m\textsuperscript{2}}%


\begin{equation}
\label{e:glauert}
\tau=\dfrac{T'}{\rho A_{P} V_{\infty}^2}
\end{equation} \\

Assuming that a propeller blade encounters similar conditions to a conventional wing, a comparison and hypothesis can be made. In a study at West Chester University \cite{fish:11icb}, tests were done on airfoils that resembled whale fins at a 1/4 scale with and without leading edge

{\makeatletter
\let\par\@@par
\par\parshape0
\everypar{}\begin{wrapfigure}{r}{0.42\textwidth}
\includegraphics[clip=True, trim= .5cm .4cm .7cm 1.5cm, width=7.8cm]{fish_lift_to_drag}
\caption{Lift to drag ratio versus angle of attack for modified and unmodified NACA 0021 airfoil. Closed circles represent unmodified airfoil data, open circles represent modified airfoil data. Re= 500,000. Figure from The Tubercles on Humpback Whales’ Flippers: Application of Bio-Inspired Technology  \cite{fish:11icb}.}
\label{f:fish_lift_to_drag}
\end{wrapfigure} 

\noindent protrusions (see Fig.~\vref{f:fish_lift_to_drag}). The Reynolds number in this test was 500,000. It can be seen that the lift to drag ratio of the flipper with tubercles was a few percent higher at angles of attack between 5$^{\circ}$ and 8$^{\circ}$. This could correlate to an ideal cruising condition where the propeller would operate at a higher efficiency. At angles of attack between 8$^{\circ}$ and 12$^{\circ}$, there was a decrease in the lift to drag ratio. However, at angles of attack between 13$^{\circ}$ and 17$^{\circ}$ the lift to drag ratio was almost twice as high. This could correlate to takeoff and climb conditions where a higher thrust would be beneficial. 

A very similar study was done at The University of Adelaide in 2012 \cite{hansen:12ua}. Reynolds number was 120,000 and the lift to drag ratio was reported for varying angles of attack. In this study there was a two percent decrease in the lift to drag coefficient at angles of attack between 5$^{\circ}$ and 12$^{\circ}$, where as in the previous study there was a two percent increase. This could indicate that Reynolds number or the variation of leading edge protrusion size and spacing could influence the lift to drag ratio and in turn the efficiency and operation envelope. For angles of attack between 13$^{\circ}$ and 17$^{\circ}$ the lift to drag ratio was again almost twice as high. 

In order to test the hypothesis that there could be greater efficiency at cruising speeds and higher thrust at takeoff speeds, physical propellers were manufactured and tested. Although the Reynolds number would not be the same, the average sizing and spacing of the humpback whale tubercles could be applied to the test propeller and result in similar effects on the fluid flow. A range of leading edge protrusion sizes and spacings was chosen to cover a relatively broad range of possibilities as the effects of propeller rotation and lower Reynolds number was unknown.

\section{Design of Experiments}
\label{Design of Experiment}

\subsection{Propeller Type Selection}
To test the effects of the addition of leading edge protrusions, a base propeller was needed. Test facilities and manufacturing costs dictated the size of the propeller and a 5.5 inch diameter and 4 inch pitch propeller was chosen, typical of small aircraft such as the Aeronautics Defense Orbiter or the AeroVironment RQ-11 Raven. This propeller had a calculated Reynolds number range of 2.9$\times 10^4$ to 5.3$\times 10^4$ with the variation due to the dynamic wind tunnel speed. The relative airspeed for the Reynolds number was calculated at 3/4 of the radius of the propeller and included the rotational velocity and wind tunnel velocity as seen by the propeller. The Reynolds number and the total velocity were defined as follows in Eqs.~\eqnref{e:Reynolds} and \eqnref{e:Vtotal}

\begin{equation}
\label{e:Reynolds}
Re_{0.75}=\dfrac{\rho W c_{0.75}}{\mu}
\end{equation}


\nomenclature{$c_{0.75}$}{chord length at 75\% of the propeller radius, m}%
\nomenclature{$\mu$}{absolute viscosity, kg/(m-s)}%

\begin{equation}
\label{e:Vtotal}
W=\sqrt{(V_{\infty}')^2+(2 \pi n R_{0.75})^2}
\end{equation}\



\nomenclature{$R_{0.75}$}{75\% of the propeller radius, m}%
\nomenclature{$W$}{blade relative velocity, m/s}


To withstand the dynamic loads from rotation, a stiff and strong material was needed. Glass filled nylon, 3-D printed by selective laser sintered (SLS) printing, was found to be the least expensive while meeting the required stiffness and strength requirements. 
\subsection{Propeller Aerodynamic Design}
\label{Propeller Aerodynamic Design}



With the diameter and pitch of the propeller chosen, a simple propeller geometry was needed as the base for the leading edge protrusions. As the study was purely comparative in nature the simple design used for full scale wood propellers was chosen.

{\makeatletter
\let\par\@@par
\par\parshape0
\everypar{}\begin{figure}[h]
\centerline{\includegraphics[clip=True, trim= 2.4cm 9.2cm 2.4cm 9cm, width=.55\textwidth]{chord_twist_distribution}}
\caption{Classic wood propeller pitch, chord length, and thickness distribution.}
\label{f:chord_twist_distribution}
\end{figure}

\begin{wrapfigure}{r}{0.3\textwidth}
\includegraphics[clip=True, trim= .4cm .4cm .4cm .8cm, width = 0.30\textwidth]{prop_sections}
\caption{Control propeller with airfoil sections represented.}
\label{f:control_propeller}
\end{wrapfigure}


Several open source tools were used with the Clark Y airfoil to define each cross section's location, scale, and angle. A spreadsheet\footnote{spreadsheet from \href{http://www.nmine.com/propeller.htm}{www.nmine.com/propeller.htm}, accessed 11/25/15} shown in Fig.~\vref{f:chord_twist_distribution} was used that defined each of the classic wooden propeller cross section's pitch, chord length, and thickness distribution as functions of propeller radius. An airfoil plotter\footnote{cross section points defined from \href{http://airfoiltools.com/plotter/index?airfoil=clarky-il}{www.airfoiltools.com}, accessed 11/21/15} was used to create the points of each cross section that corresponded with the parameters in the spreadsheet. 
The points of each cross section were then imported into a CAD modeling program, and surface modeling was used to construct the propeller shown in Fig.~\vref{f:control_propeller}. This propeller was used as the control propeller for the experiments as well as the base for the leading edge protrusion additions.



\subsection{Leading Edge Protrusion Size and Spacing}
\label{Leading Edge Protrusion Size and Spacing} \


 As described in Section \ref{Hypothesized Full Range of Affects on Propellers}, the leading edge protrusion size and spacing was hypothesized to affect the lift to drag ratio and in turn the propeller's envelope of operation and efficiency. There needed to be a range of sizings and spacings that would cover a broad range of possibilities while maintaining a reasonable scope. Three sizes and three spacings were chosen, resulting in nine test propellers. These numbers were chosen based on the humpback whale's average tubercle size and spacings, with the extent of the variations being chosen from space limitations. The medium size and medium spacing represented the humpback whale's average tubercle size and spacing. 

An oblate spheroid was chosen in order to quantify the protrusions added to the control propeller. The height was defined as 2/3 the width, similar to the humpback whale tubercle sizing. The ranges of widths were sized at 5, 6.25, and 8 percent of the effective propeller radius, or distance from root to tip. The spacings between protrusions were defined as 7.14, 8.33, and 10 percent of the effective propeller radius, which was 2.5 inches in this study. The medium spacing and medium width, representing the average humpback whale tubercle sizing and spacing, fit 12 protrusions along the blade with a width of 6.25\% of the blade's effective radius. The smallest spacing and the largest width were chosen to fit 14 protrusions touching side by side. The largest spacing and the smallest width were chosen to fit 10 protrusions on the blade and be 20\% smaller than the medium width.

\begin{figure}[h]
\centerline{\includegraphics[clip=True, trim= 1cm 7.2cm 7cm 3.55cm, width=.99\textwidth]{size_and_spacing}}
\caption{Graphical representation of protrusion sizes and spacings. Dimensions are given as percentages of the effective propeller radius or distance from root to tip.}
\label{f:sizes_and_spacings}
\end{figure}

\noindent Fig.~\vref{f:sizes_and_spacings} gives a graphic representation of the final design of experiment. Each oblate spheroid was set such that 25\% of the width was exposed past the leading edge. Vertical positioning was at the intersection of the chord line with the leading edge, and the angle was parallel to the chord line.


\section{Testing}
\label{Propeller Testing}

\subsection{Required Data}
\label{Required Data Plots}

Proper analysis of the hypothesized effects as discussed in Section \ref{Hypothesized Full Range of Affects on Propellers} required two types of data to be collected. The first was efficiency versus advance ratio, which gave insight into the efficiency effects over a wide range of angles of attack represented by the advance ratio. The second was coefficient of thrust versus advance ratio, which gave insight into the thrust effects over a wide range of angles of attack. 

For the efficiency versus advance ratio plot, the efficiency was defined as the measured power out divided by the measured power in, as defined in Eq.~\eqnref{e:efficiency}. \\

\begin{equation}
\label{e:efficiency}
\eta_{p}=\dfrac{P_{out}}{P_{in}}=\dfrac{T'{\times}V_{\infty}'}{E{\times}I}
\end{equation}

\nomenclature{$\eta_{p}$}{power system efficiency}%
\nomenclature{$P_{out}$}{power out, Watts}%
\nomenclature{$P_{in}$}{power in, Watts}%
\nomenclature{$E$}{voltage, volts}%
\nomenclature{$I$}{current, amps}%
\nomenclature{$T'$}{corrected thrust, N}%

\noindent \\ 

\noindent For the coefficient of thrust versus advance ratio plot, the coefficient of thrust was defined as in Eq.~\eqnref{e:coefficient_of_thrust}.

\begin{equation}
\label{e:coefficient_of_thrust}
C_T=\dfrac{T'}{\rho{n}^2D^4}
\end{equation}

\nomenclature{$C_T$}{coefficient of thrust}%
\nomenclature{$\rho$}{fluid density, kg/m\textsuperscript{3}}%
\nomenclature{$n$}{rotational speed, Hz}%

\noindent A cowling was constructed and temporarily used but induced significant error into the measurements due to its close proximity to the propeller. A corrected thrust was used instead which accounts for the measured drag on the test stand from free stream flow without the propeller attached. The corrected thrust is defined in Eq.~\eqnref{e:corrected_thrust} and the curve relating the airspeed to the drag is shown in Fig.~\ref{f:calibrationd}. 

\begin{equation}
\label{e:corrected_thrust}
T'=T-F_{D}
\end{equation} 

\nomenclature{$T$}{measured thrust, N}%
\nomenclature{$F_{D}$}{test stand drag as measured, N}%



To collect standardized data, several test variables were controlled in specific ways. The propeller rotational speed ($n$) was held constant to maintain a relatively consistent motor efficiency. This induced a small change in the Reynolds number which changed the 8,000 RPM case from 2.9$\times 10^4$ to 3.0$\times 10^4$ and changed the 14,000 RPM case from 4.9$\times 10^4$ to 5.3$\times 10^4$. This small change in the Reynolds number should only have a minimal effect on the data. The system voltage ($E$) was held constant to maintain consistent efficiency in the motor and motor controller. The propeller diameter ($D$) and the fluid density ($\rho$) were unchanged. The two remaining variables, system current ($I$) and propeller thrust ($T$), were measured.



\subsection{Instrumentation}
\label{Instrumentation}

For accurate data collection, proper instrumentation was required. A 16 bit DAQ was used to measure the voltage outputs of all sensors, and calibration data was recorded from known inputs. Calibration curves were created using least squares regression and later used to interpret the sensor voltage outputs (see Fig.~\vref{f:calibration}(a-c)). \\\

\begin{figure}[!htb]
\begin{subfigmatrix}{2}% number of columns
\centering
\subfigure[Linear least squares regression of current calibration data.]{\includegraphics[clip=True, trim= 2.4cm 9.7cm 3.45cm 9.4cm, width=0.45\textwidth]{current_calibration}\label{f:calibrationa}}
\subfigure[Linear least squares regression of strain calibration data. ]{\includegraphics[clip=True, trim= 2.4cm 9.7cm 4.2cm 9.4cm, width=0.45\textwidth]{thrust_calibration}\label{f:calibrationb}}\\\
\subfigure[Second order least squares regression of airspeed calibration data. ]{\includegraphics[clip=True, trim= 2.4cm 9.7cm 3.2cm 9.4cm, width=0.45\textwidth]{airspeed_calibration}\label{f:calibrationc}}
\subfigure[Second order least squares regression of test stand drag data. ]{\includegraphics[clip=True, trim= 2.4cm 9.7cm 3.2cm 9.4cm, width=0.45\textwidth]{drag_calibration}\label{f:calibrationd}}
\end{subfigmatrix}
\caption{Calibration curves for current, strain, airspeed, and test stand drag.}
\label{f:calibration}
\end{figure} 

A Hall effect sensor was used to measure system current ($I$), strain gages were used to measure propeller thrust ($T$), and a pitot tube with pressure transducer was used to measure the controlled wind tunnel velocity ($V$). System voltage ($E$) was held constant by a power supply. Labview was set to record data points consisting of 10 seconds of raw sensor voltages at 400 hertz. The raw data was averaged into points, scaled by the calibration curves, non-dimensionalized, and plotted. 

\begin{wrapfigure}{r}{0.50\textwidth}
\includegraphics[clip=True, trim= 7cm 9.5cm 7cm 3cm,width=.5\textwidth]{test_stand}
\caption{Test stand while in operation}
\label{f:test_stand}
\end{wrapfigure}

To hold the propeller rotational speed ($n$) constant, a 10 bit micro controller running a PID (proportional-integral-derivative) control algorithm was used. The PID algorithm was defined by the open source PID library from $Arduino$. The microcontroller was set up to measure the motor RPM from the voltage pulses coming from the three leads of the brushless motor using an Eagletree rpm sensor. Pulses were counted, polled, and reset at a set interval. Using a strobe tachometer, the relation between the rpm and the number of pulses per interval was found to be linear and the relation was implemented into the PID controller. The proportional, derivatives, and integral gains were chosen to give an adequate response time without oscillation. The result was a constant RPM with a 5 second or less response regardless of load until the maximum system capacity. A photograph of the test stand and motor can be seen in Fig.~\vref{f:test_stand}.



%\newpage
\subsection{Validation Test}
\label{Validation Test}

To validate the test stand and process, a propeller of known performance\cite{Brezina:13aiaa} was tested. The thrust and efficiency curves were compared and found to be similar. However, it should be noted that because of the comparative nature of the final data, power was not measured at the motor shaft by torque. Power was measured at the electrical input to the motor controller and the efficiency data is of the entire system. For validation purposes, a constant correction factor due to motor inefficiency was applied to the efficiency curves only. For the 14k, 12k and 8k RPM efficiency cases, scaling factors of 0.56, 0.57, and 0.48 were applied respectively. These factors are similar to motor efficiencies in another study for similar throttle inputs\cite{phelps}. Results for thrust coefficient and efficiency can be seen in Fig.~\vref{f:validation}. 

\begin{figure}[!htb]
\begin{subfigmatrix}{2}% number of columns
\centering
\subfigure[APC 6x4 coefficient of thrust validation results plotted against known performance\cite{Brezina:13aiaa}.]{\includegraphics[clip=True, trim= .4cm 0cm .5cm 0cm, width=0.49\textwidth]{validation_thrust}}
\subfigure[APC 6x4 efficiency validation results plotted against known performance\cite{Brezina:13aiaa}, scaled to account for electrical inefficiency]{\includegraphics[clip=True, trim= .5cm 0cm .4cm 0cm, width=0.49\textwidth]{validation_efficiency}}
\end{subfigmatrix}
\caption{Coefficient of thrust and efficiency curves for validation test.}
\label{f:validation}
\end{figure} 


For the efficiency plot, the trends and values followed closely to the known data and showed that the electrical system inefficiency was constant up to an advance ratio of about .4 for all of the rotational speeds tested. Above the advance ratio of .4 the efficiency seemed to drop off. Physically, this was when the throttle input and in turn the current required to maintain the specified RPM also dropped off significantly where as previously it had remained relatively constant. This type of motor efficiency curve (decreasing throttle input yielding lower efficiency) has also been previously documented \cite{phelps}. The coefficient of thrust plot was not affected by the electrical scaling factor, and in all cases showed similar results. The scaling factors were not applied to the thrust coefficient curve or the final data in the following sections. 

\FloatBarrier


\section{Data Analysis}
\label{Data Analysis}

The results of the testing showed in all cases that no protrusion sizing or spacing had a positive effect above the error of the measurement system. It was also shown that protrusion spacing had a negligible effect for each protrusion size. An example with the small protrusion size can be seen in Fig.~\vref{f:spacing_no_matter}. This lack of variation was consistent in all of the protrusion sizes and as such, only the medium protrusion spacing was plotted for the rest of the report.\\\\

\begin{figure}[!htb]
\begin{subfigmatrix}{2}% number of columns
\centering
\subfigure[Small protrusion size coefficient of thrust at 8,000 RPM]{\includegraphics[clip=True, trim= .4cm 0cm .5cm 0cm, width=0.49\textwidth]{small_eight_thrust}}
\subfigure[Small protrusion size efficiency at 8,000 RPM]{\includegraphics[ clip=True, trim= .4cm 0cm .5cm 0cm,width=0.49\textwidth]{small_eight_efficiency}}
\end{subfigmatrix}
\caption{Coefficient of thrust and efficiency curves showing spacing variations produce negligible effects.}
\label{f:spacing_no_matter}
\end{figure}

Tests on different days were done with the same physical setup and data analysis code and yielded similar results. The control propeller with no protrusions was tested at the beginning and ending of the data collection in order to show any drift or changes in hardware. The following sections show the effects seen in efficiency and coefficient of thrust.

\newpage
\subsection{Efficiency Analysis}
\label{ Efficiency Analysis}

In the efficiency analysis, three cases are plotted; 8,000, 12,000, and 14,000 RPM. Each of these cases show data for varying protrusion size and a constant medium protrusion spacing. The 14,000 RPM case seems to show the largest relative decrease over all the advance ratios tested as seen in Fig.~\ref{f:efficiencyc}. In all three cases there was a lessened effect in advance ratios between 0 and 0.2 and 0.7 and 0.8. As a general trend, it seemed that increasing either protrusion size or rotational speed lowered efficiency.\\\\\\


\begin{figure}[!htb]
\begin{subfigmatrix}{2}% number of columns
\subfigure[ 8,000 RPM medium protrusion spacing efficiency analysis with two data sets per propeller.]{\includegraphics[clip=True, trim= .5cm 0cm .1cm 0cm, width=0.49\textwidth]{mixed_eight_efficiency}\label{f:efficiencya}}
\subfigure[12,000 RPM medium protrusion spacing efficiency analysis with two data sets per propeller.]{\includegraphics[clip=True, trim= .5cm 0cm 0cm 0cm, width=0.49\textwidth]{mixed_twelve_efficiency}\label{f:efficiencyb}}
\subfigure[ 14,000 RPM medium protrusion spacing efficiency analysis with two data sets per propeller.]{\includegraphics[clip=True, trim= .5cm 0cm 0cm -2cm, width=0.49\textwidth]{mixed_fourteen_efficiency}\label{f:efficiencyc}}
\end{subfigmatrix}
\caption{Efficiency analysis for medium spacing.}
\label{f:efficiency}
\end{figure} 



\FloatBarrier
\newpage
\subsection{ Thrust Analysis}
\label{Thrust Analysis}

Similar to the efficiency analysis, three cases are plotted; 8,000, 12,000, and 14,000 RPM. The medium protrusion spacing and varying protrusion sizes were plotted. The 14,000 RPM case again seems to show the largest relative decrease as seen in Fig.~\ref{f:thrustc}. A constant decrease in coefficient of thrust was seen over all of the advance ratios and protrusion sizes tested. Though not plotted, the small and large protrusion spacing showed little variation from the medium spacing as previously indicated. As a general trend, increasing protrusion size had a large negative effect on the coefficient of thrust, and increasing rotational speed had a small negative effect on the coefficient of thrust.\\\\\

\begin{figure}[!htb]
\begin{subfigmatrix}{2}% number of columns
\subfigure[8,000 RPM medium protrusion spacing coefficient of thrust analysis with two data sets per propeller.]{\includegraphics[clip=True, trim= .5cm 0cm 0cm 0cm, width=0.49\textwidth]{mixed_eight_thrust}\label{f:thrusta}}
\subfigure[12,000 RPM medium protrusion spacing coefficient of thrust analysis with two data sets per propeller.]{\includegraphics[clip=True, trim= .5cm 0cm 0cm 0cm, width=0.49\textwidth]{mixed_twelve_thrust}\label{f:thrustb}}
\subfigure[14,000 RPM medium protrusion spacing coefficient of thrust analysis with two data sets per propeller.]{\includegraphics[clip=True, trim= .5cm 0cm 0cm -2cm, width=0.49\textwidth]{mixed_fourteen_thrust}\label{f:thrustc}}
\end{subfigmatrix}
\caption{Coefficient of thrust analysis for medium protrusion spacing.}
\label{f:thrust}
\end{figure} 



\FloatBarrier
\section{Conclusions and Future Work}
\label{Conclusions and Future Work}

For the base propeller tested, it has been shown that leading edge protrusions of the size, spacing, and type in this report only contributed to degraded performance. In the case of the small protrusion size and slowest RPM, it was shown that there were negligible negative effects with little or no positive effects at any of the advance ratios tested. For the cases mentioned in the introduction of this report such as an increased envelope of operation, performance, or endurance for small aircraft, no positive effects would be seen if the propellers in this study were used.

The main difference between the tests done in this study and the other studies previously referenced is the operating Reynolds number. The two studies cited in Sec.~\ref{Hypothesized Full Range of Affects on Propellers} did testing at a Reynolds number of 5$\times 10^5$ and 1.2$\times 10^5$ respectively. The inherent propeller size and operating conditions in this study gave a Reynolds number of about 5$\times 10^4$, an order of magnitude lower. Because Reynolds number is the relation of dynamic forces to viscous forces, it is possible that the relatively larger viscous forces are damping out the induced flow patters seen in studies with higher Reynolds numbers. If true, this would explain how the leading edge protrusions in the tests done in this report seem to only contribute to drag. It is possible that testing at higher Reynolds numbers will produce positive results. 

If the operating Reynolds number is the only limiting factor, it is unlikely that this type of modification will be feasible for small aircraft propellers typical of unmanned aerial vehicles. An application where there is an increased chord length or velocity due to rotation, such as a larger aircraft propeller, may see positive results.    

Possible future work would be to either physically prototype leading edge protrusions on larger propellers, or create a validated CFD model and experiment with varying propeller sizes and leading edge geometries. Alternate geometry with the same propeller size and operating conditions may also yield different results. For future work, the data sets used to create the efficiency and coefficient of thrust plots as well as the solid model files for the propellers can be found on \href{http://flow.byu.edu/publications/}{http://flow.byu.edu/publications/}.

\section*{Acknowledgments}
We, the authors, would like to thank McKay Graff for his timely help that enabled the testing to be completed on time.  We would also like to acknowledge Dr.~Jerry Bowman and Larry Moore for their excellent engineering advice and help.  Finally, for invaluable technical help and advice, we would like to thank Kevin Cole and Nick Hawkins.  

\FloatBarrier
% produces the bibliography section when processed by BibTeX
\bibliography{Sources}
\bibliographystyle{aiaa}
\end{document}

% - Kevin R Moore 5/16/15 $ -



