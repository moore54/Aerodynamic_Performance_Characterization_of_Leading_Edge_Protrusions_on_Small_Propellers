\begin{thenomenclature} 

 \nomgroup{A}

  \item [{$\eta_{p}$}]\begingroup power system efficiency\nomeqref {6}
		\nompageref{6}
  \item [{$\mu$}]\begingroup absolute viscosity, kg/(m-s)\nomeqref {4}
		\nompageref{4}
  \item [{$\rho$}]\begingroup fluid density, kg/m\textsuperscript{3}\nomeqref {7}
		\nompageref{6}
  \item [{$\tau$}]\begingroup Glauert correction variable\nomeqref {2}
		\nompageref{3}
  \item [{$A_{P}$}]\begingroup propeller disk area, m\textsuperscript{2}\nomeqref {2}
		\nompageref{3}
  \item [{$A_{WT}$}]\begingroup wind tunnel cross sectional area, m\textsuperscript{2}\nomeqref {2}
		\nompageref{3}
  \item [{$C_T$}]\begingroup coefficient of thrust\nomeqref {7}
		\nompageref{6}
  \item [{$c_{0.75}$}]\begingroup chord length at 75\% of the propeller radius, m\nomeqref {4}
		\nompageref{4}
  \item [{$D$}]\begingroup propeller diameter, m\nomeqref {1}
		\nompageref{3}
  \item [{$E$}]\begingroup voltage, volts\nomeqref {6}\nompageref{6}
  \item [{$F_{D}$}]\begingroup test stand drag as measured, N\nomeqref {8}
		\nompageref{6}
  \item [{$I$}]\begingroup current, amps\nomeqref {6}\nompageref{6}
  \item [{$J$}]\begingroup advance ratio\nomeqref {1}\nompageref{3}
  \item [{$n$}]\begingroup rotational speed, Hz\nomeqref {7}
		\nompageref{6}
  \item [{$P_{in}$}]\begingroup power in, Watts\nomeqref {6}
		\nompageref{6}
  \item [{$P_{out}$}]\begingroup power out, Watts\nomeqref {6}
		\nompageref{6}
  \item [{$R_{0.75}$}]\begingroup 75\% of the propeller radius, m\nomeqref {5}
		\nompageref{4}
  \item [{$T$}]\begingroup measured thrust, N\nomeqref {8}
		\nompageref{6}
  \item [{$T'$}]\begingroup corrected thrust, N\nomeqref {6}
		\nompageref{6}
  \item [{$V'_{\infty}$}]\begingroup corrected free stream velocity, m/s\nomeqref {1}
		\nompageref{3}
  \item [{$V_{\infty}$}]\begingroup measured free stream velocity, m/s\nomeqref {2}
		\nompageref{3}
  \item [{$W$}]\begingroup blade relative velocity, m/s\nomeqref {5}
		\nompageref{4}

\end{thenomenclature}
