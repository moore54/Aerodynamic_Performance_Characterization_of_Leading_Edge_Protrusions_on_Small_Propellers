\nomenclatureentry{a$J$@[{$J$}]\begingroup advance ratio\nomeqref {1}|nompageref}{3}
\nomenclatureentry{a$V'_{\infty}$@[{$V'_{\infty}$}]\begingroup corrected free stream velocity, m/s\nomeqref {1}|nompageref}{3}
\nomenclatureentry{a$D$@[{$D$}]\begingroup propeller diameter, m\nomeqref {1}|nompageref}{3}
\nomenclatureentry{a$\tau$@[{$\tau$}]\begingroup Glauert correction variable\nomeqref {2}|nompageref}{3}
\nomenclatureentry{a$V_{\infty}$@[{$V_{\infty}$}]\begingroup measured free stream velocity, m/s\nomeqref {2}|nompageref}{3}
\nomenclatureentry{a$A_{WT}$@[{$A_{WT}$}]\begingroup wind tunnel cross sectional area, m\textsuperscript{2}\nomeqref {2}|nompageref}{3}
\nomenclatureentry{a$A_{P}$@[{$A_{P}$}]\begingroup propeller disk area, m\textsuperscript{2}\nomeqref {2}|nompageref}{3}
\nomenclatureentry{a$c_{0.75}$@[{$c_{0.75}$}]\begingroup chord length at 75\% of the propeller radius, m\nomeqref {4}|nompageref}{4}
\nomenclatureentry{a$\mu$@[{$\mu$}]\begingroup absolute viscosity, kg/(m-s)\nomeqref {4}|nompageref}{4}
\nomenclatureentry{a$R_{0.75}$@[{$R_{0.75}$}]\begingroup 75\% of the propeller radius, m\nomeqref {5}|nompageref}{4}
\nomenclatureentry{a$W$@[{$W$}]\begingroup blade relative velocity, m/s\nomeqref {5}|nompageref}{4}
\nomenclatureentry{a$\eta_{p}$@[{$\eta_{p}$}]\begingroup power system efficiency\nomeqref {6}|nompageref}{6}
\nomenclatureentry{a$P_{out}$@[{$P_{out}$}]\begingroup power out, Watts\nomeqref {6}|nompageref}{6}
\nomenclatureentry{a$P_{in}$@[{$P_{in}$}]\begingroup power in, Watts\nomeqref {6}|nompageref}{6}
\nomenclatureentry{a$E$@[{$E$}]\begingroup voltage, volts\nomeqref {6}|nompageref}{6}
\nomenclatureentry{a$I$@[{$I$}]\begingroup current, amps\nomeqref {6}|nompageref}{6}
\nomenclatureentry{a$T'$@[{$T'$}]\begingroup corrected thrust, N\nomeqref {6}|nompageref}{6}
\nomenclatureentry{a$C_T$@[{$C_T$}]\begingroup coefficient of thrust\nomeqref {7}|nompageref}{6}
\nomenclatureentry{a$\rho$@[{$\rho$}]\begingroup fluid density, kg/m\textsuperscript{3}\nomeqref {7}|nompageref}{6}
\nomenclatureentry{a$n$@[{$n$}]\begingroup rotational speed, Hz\nomeqref {7}|nompageref}{6}
\nomenclatureentry{a$T$@[{$T$}]\begingroup measured thrust, N\nomeqref {8}|nompageref}{6}
\nomenclatureentry{a$F_{D}$@[{$F_{D}$}]\begingroup test stand drag as measured, N\nomeqref {8}|nompageref}{6}
